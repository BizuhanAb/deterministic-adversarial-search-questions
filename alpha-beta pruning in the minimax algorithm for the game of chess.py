import math

def print_board(board):
    for row in board:
        print(" | ".join(row))
        print("-" * 33)

def is_terminal(board):
    # Check if the game is over
    return any(row.count(' ') == 0 for row in board)

def heuristic_evaluation(board):
    # This is a simple heuristic function.
    # You can modify it to create a more sophisticated one.
    score = 0
    for row in board:
        for piece in row:
            if piece == 'O':
                score += 1
            elif piece == 'X':
                score -= 1
    return score

def minimax(board, depth, alpha, beta, is_maximizing_player):
    if depth == 0 or is_terminal(board):
        return heuristic_evaluation(board)

    if is_maximizing_player:
        max_eval = float('-inf')
        for i in range(8):
            for j in range(8):
                if board[i][j] == ' ':
                    board[i][j] = 'O'
                    eval = minimax(board, depth - 1, alpha, beta, False)
                    board[i][j] = ' '
                    max_eval = max(max_eval, eval)
                    alpha = max(alpha, eval)
                    if beta <= alpha:
                        break
        return max_eval
    else:
        min_eval = float('inf')
        for i in range(8):
            for j in range(8):
                if board[i][j] == ' ':
                    board[i][j] = 'X'
                    eval = minimax(board, depth - 1, alpha, beta, True)
                    board[i][j] = ' '
                    min_eval = min(min_eval, eval)
                    beta = min(beta, eval)
                    if beta <= alpha:
                        break
        return min_eval

def get_best_move(board, depth):
    best_val = float('-inf')
    best_move = (-1, -1)

    for i in range(8):
        for j in range(8):
            if board[i][j] == ' ':
                board[i][j] = 'O'
                move_val = minimax(board, depth - 1, float('-inf'), float('inf'), False)
                board[i][j] = ' '
                if move_val > best_val:
                    best_move = (i, j)
                    best_val = move_val

    return best_move

def play_chess_game():
    board = [[' ' for _ in range(8)] for _ in range(8)]

    while not is_terminal(board):
        print_board(board)

        # Assuming 'X' represents the player
        row = int(input("Enter the row (0-7): "))
        col = int(input("Enter the column (0-7): "))
        if board[row][col] == ' ':
            board[row][col] = 'X'
        else:
            print("Invalid move. Try again.")
            continue

        if is_terminal(board):
            break

        # AI's turn
        ai_row, ai_col = get_best_move(board, depth=3)
        board[ai_row][ai_col] = 'O'

    print_board(board)
    print("Game over.")

if __name__ == "__main__":
    play_chess_game()
